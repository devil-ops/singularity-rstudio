#!/bin/bash
NEXTPORT=`comm -23 <(seq 8787 9000 | sort) <(ss -tan | awk '{print $4}' | cut -d':' -f2 | grep "[0-9]\{1,5\}" | sort -u) | sort -n | head -n 1`
echo "running at port "$NEXTPORT
echo "www-port="$NEXTPORT > /etc/rstudio/rserver.conf
rserver
